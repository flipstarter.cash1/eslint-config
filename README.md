# @generalprotocols/eslint-config
This repository contains the official General Protocols ESLint configuration for JavaScript and TypeScript projects within the General Protocols organization. It is based on and extends the official Airbnb ESLint configuration and its TypeScript equivalent.

Jonathan Silverblood and Rosco Kalis created this file on 2020-06-07 by going through the Airbnb rules and changing rules where one or both of us disagreed.

One of the changes is that we enabled some rules that are currently disabled by Airbnb but planned to be enabled. When these rules get enabled by Airbnb they can be removed from this configuration. We also put some rules that we were not certain of on `"warn"`. Ideally these rules will either be switched to `"off"` if they lead to difficulties or eventually switched to `"error"`.

The configuration should change as little as possible. But changes can be requested as long as they are accompanied by a very good justification.

## JavaScript projects
Install dependencies. ESlint plugins also need to be installed.

```
npm install eslint \
    eslint-plugin-import \
    https://gitlab.com/GeneralProtocols/eslint-config \
    --save-dev
```

Extend this config in your ESLint config file:

```js
module.exports =
{
    extends: [ '@generalprotocols/eslint-config' ],
};
```

## TypeScript projects
Install dependencies. ESlint plugins also need to be installed.

```
npm install eslint \
    eslint-plugin-import \
    @typescript-eslint/eslint-plugin \
    https://gitlab.com/GeneralProtocols/eslint-config \
    --save-dev
```

Extend this config in your ESLint config file:

```js
module.exports =
{
    extends: ['@generalprotocols/eslint-config/typescript'],
    parserOptions:
    {
        project: './tsconfig.json',
    },
};
```
