const { rules: baseRules } = require('./index')

module.exports = {
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint'],
    extends: ['airbnb-typescript/base', './index.js'],
    parserOptions: {
        project: './tsconfig.json',
        ecmaVersion: 2018, // Allows the parsing of modern ECMAScript features
        sourceType: 'module' // Allows the use of imports
    },
    rules: {
        // // ADDITIONAL TYPESCRIPT RULES
        // Enforce semicolons in interfaces and type declarations
        // https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-delimiter-style.md
        '@typescript-eslint/member-delimiter-style': ['error'],

        // Every function needs to be explicit about its return type
        // https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/explicit-function-return-type.md
        '@typescript-eslint/explicit-function-return-type': ['error', { allowExpressions: true }],

        // Empty TypeScript constructors are not useless and should be allowed
        // https://eslint.org/docs/rules/no-useless-constructor
        // https://eslint.org/docs/rules/no-empty-function
        'no-useless-constructor': ['off'],
        'no-empty-function': ['off'],

        // Allow JS-style requires for backward compatibility
        // https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-var-requires.md
        '@typescript-eslint/no-var-requires': ['off'],

        // CONVERT JS RULES TO TS RULES
        'brace-style': 'off',
        '@typescript-eslint/brace-style': baseRules['brace-style'],

        'dot-notation': 'off',
        '@typescript-eslint/dot-notation': baseRules['dot-notation'],

        'indent': 'off',
        '@typescript-eslint/indent': baseRules['indent'],

        'keyword-spacing': 'off',
        '@typescript-eslint/keyword-spacing': baseRules['keyword-spacing'],

        'lines-between-class-members': 'off',
        '@typescript-eslint/lines-between-class-members': baseRules['lines-between-class-members'],

        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': baseRules['no-unused-vars'],

        'no-shadow': 'off',
        '@typescript-eslint/no-shadow': baseRules['no-shadow'],

        'space-before-function-paren': 'off',
        '@typescript-eslint/space-before-function-paren': baseRules['space-before-function-paren'],

        // This breaks because of the two different extensions
        // It is safe to disable these, since they are independently checked by TS
        'import/extensions': ['off'],
        'import/no-unresolved': ['off'],
        'import/no-extraneous-dependencies': ['off'],

        // This is checked by TypeScript independently
        'no-undefined': ['off']
    }
}
